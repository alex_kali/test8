export const test10 = () => {
  console.log('start'); // 1

  setTimeout(() => console.log('timeout'), 0); // 7

  new Promise((resolve, reject) => {

    console.log('promise constructor'); // 2

    reject();
  }).then(() => console.log('promise'))
    .catch(() => console.log('promise1')) //4
    .catch(() => console.log('promise2'))
    .then(() => console.log('promise3')) //5
    .then(() => console.log('promise4')) //6

  console.log('final'); // 3


  function print() {
    console.log(1); // 3
  }
  async function foo(){
    console.log(2); // 2
    await print();
    console.log(3); //6
  }
  console.log(4); // 1
  foo();
  console.log(5); // 4

}