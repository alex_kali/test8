export const test6 = () => {
  const operations = [
    { "date": "2017-07-31", "amount": "5422" },
    { "date": "2017-06-30", "amount": "5220" },
    { "date": "2017-05-31", "amount": "5365" },
    { "date": "2017-08-31", "amount": "5451" },
    { "date": "2017-09-30", "amount": "5303" },
    { "date": "2018-03-31", "amount": "5654" },
    { "date": "2017-10-31", "amount": "5509" },
    { "date": "2017-12-31", "amount": "5567" },
    { "date": "2018-01-31", "amount": "5597" },
    { "date": "2017-11-30", "amount": "5359" },
    { "date": "2018-02-28", "amount": "5082" },
    { "date": "2018-04-14", "amount": "2567" }
  ];

  function sortOperations(operations: Array<{date: string, amount: string}>) {
    const result = {} as any
    operations.sort((item1, item2) => new Date(item1.date).getTime() - new Date(item2.date).getTime())
    for(let i of operations){
      const splitData = i.date.split('-')
      if(result[splitData[0]]){
        result[splitData[0]].push(splitData[1] + '-' + splitData[2])
      }else{
        result[splitData[0]] = [splitData[1] + '-' + splitData[2]]
      }
    }
    return result
  }

  console.log(sortOperations(operations))
}