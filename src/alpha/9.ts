export const test9 = () => {
  var a = {
    firstName: 'Bill',
    lastName: 'Ivanov',
    sayName: function() {
      console.log('Hi! I am ' + this?.firstName);
    },
    sayLastName: () => {
      console.log('Hi! I am ' + (this as any)?.lastName);
    }
  };

  a.sayName(); // bill

  var b = a.sayName;

  b(); // undefined

  a.sayName.bind({ firstName: 'Boris1' })(); // boris
  a.sayName();// bill
  a.sayLastName(); // undefined



  a.sayName.bind({ firstName: 'Boris2' }).bind({ firstName: 'Tom' })(); // tom
  a.sayName()
  a.sayLastName.bind({ lastName: 'Petrov' })(); // undefined

}