export const test13 = () => {
  // var foo = 1;
  // (function f() {
  //   if (foo) {
  //     var foo = 2
  //   }
  //
  //   console.log(foo) // undefined // второе переопределение ломает работу // собенности работы var
  // })();


  // Task 2

  // console.log(this); // undefined // вызывается вне обьекта
  //
  // function foo() {
  //   console.log(this); // undefined // вызывается вне обьекта
  // }
  //
  //
  // function foo2() {
  //   'use strict'
  //   console.log(this); // undefined // вызывается вне обьекта
  // };
  //
  // foo();
  // foo2();

  // Task 3


  // const square = {
  //   side: 5,
  //   area() {
  //     return this.side * this.side;
  //   },
  //   perimeter: () => 4 * this.side
  // };
  //
  // console.log(square.area()); // 25
  // console.log(square.perimeter()); // ошибка у стрелочной функции нет this


  // Task 4


  // function foo() {
  //   console.log(this); // undefined // вызывается функция вне обекта
  // }
  //
  // function doFo(fn) {
  //   fn();
  // }
  //
  // var a = { foo: foo };
  //
  // doFo(a.foo);

  // Task 5


  // class Customer {
  //   constructor(name) {
  //     this.name = name;
  //   }
  //
  // }
  //
  // Customer.prototype.pay = function () {
  //   console.log(`Чек от месье ${this.name}`);
  // };
  //
  // const Ivan = new Customer("Ivan");
  //
  // Ivan.pay(); // ivan
  //
  // delete Customer.prototype.pay;

  // Ivan.pay(); // error

  // Task 6
  //
  // console.log(1);
  //
  // console.log(3);
  //
  // new Promise(res => {
  //   res();
  // }).then(() => console.log(4));
  //
  // setTimeout(() => console.log(2), 0);

// Task 7


  Promise.resolve(1)
    .then(x => x + 1)
    .then(x => { throw x })
    .then(x => console.log(x))
    .catch(err => console.log(err)) // 2
    .then(() => Promise.resolve(1))
    .catch(err => console.log(err))
    .then(x => console.log(x)); // 1




}