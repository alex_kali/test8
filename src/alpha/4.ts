export const test4 = () => {
  // @ts-ignore
  Array.prototype.some = function(callback:any) {
    for (let i of this) {
      if (callback(i)) return true;
    }
    return false
  }

  const array = [1,2,3]
  console.log(array.some((item) => console.log(item)))
}