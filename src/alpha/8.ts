export const test8 = () => {
  function makeCounter() {
    let count = 0
    return () => {count++; return count}
  }

  let counter = makeCounter()

  console.log(counter()) // 1
  console.log(counter()) // 2

}