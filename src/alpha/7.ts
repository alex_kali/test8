export const test7 = () => {
  var value = 2;
  function showValue() {
    console.log(`showValue ${value}`);//2
  }

  function wrapper() {
    var value = 3;
    console.log(`wrapper ${value}`);//3
    showValue();
  }

  wrapper();

}