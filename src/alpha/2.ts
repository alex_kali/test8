export const test2 = () => {
  const dog = {
    name: 'Guffi',
    sayName: function () {
      console.log(this.name);
    },
  };

  const sayName = dog.sayName;

  sayName.call(dog);

}