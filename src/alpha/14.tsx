import React, { useState } from 'react';

export function Test14() {
  const [count, setCount] = useState(0);

  const handleClick = () => {
    setCount((count) => count + 1);
    setCount((count) => count + 1);
  };

  return (
    <div className="App">
      <h1>Count: {count}</h1>
      <button
        onClick={handleClick}
      >
        Увеличить
      </button>
    </div>
  );
}
