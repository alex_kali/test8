import {useCallback, useEffect, useLayoutEffect, useRef, useState} from "react";

export const Component = () => {
  const [items, setItems] = useState<Array<{id: number, title: string}>>([]);
  const refCount = useRef(0)

  useEffect(() => {

    const listener = () => {
      setInterval(() => {
        console.log(refCount.current);
      }, 1000);
      document.removeEventListener('click', listener)
    }

    document.addEventListener('click', listener);

  }, []);


  const onClick = () => {
    setItems([...items, ...[{ id: refCount.current + 1, title: (new Date).getTime().toString() }]]);
    refCount.current = refCount.current + 1
  };

  return (
    <>
      <button onClick={onClick}>Click me!</button>
      <p>{refCount.current}</p>
      {items.map((el) => <li key={el.id}>{el.title}</li>)}
    </>
  );
};
