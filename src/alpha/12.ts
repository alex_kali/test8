export const test12 = () => {
  type ElementOf<T> = T extends (infer Element)[] ? Element : never

  type TestType = string[];

  type Some = ElementOf<TestType>; // string

}