import styled from "styled-components";

export const TimerStyled = styled.div`
  margin: 100px;
`

export const SecondStyled = styled.div`
  display: inline-block;
  font-size: 30px;
`

export const MillisecondStyled = styled.div`
  display: inline-block;
  font-size: 12px;
  margin-left: 2px;
`

export const TimerButtonStyled = styled.div`
  display: inline-block;
  width: 23px;
  height: 23px;
  background: grey;
  margin-left: 15px;
  cursor: pointer;
`

export const ResetTimerButtonStyled = styled.div`
  display: inline-block;
  width: 23px;
  height: 23px;
  background: lightblue;
  margin-left: 15px;
  cursor: pointer;
`