import React, {FC, useEffect, useRef, useState} from "react";
import {TimerButtonStyled, TimerStyled, SecondStyled, MillisecondStyled, ResetTimerButtonStyled} from "./styled";

export const Timer:FC<{time: number}> = (props) => {
  const [time, setTime] = useState(Math.ceil(props.time / 10))
  const [isActive, setIsActive] = useState(false)
  const refIsActive = useRef(isActive)

  useEffect(() => {
    setTimeout(() => {
      if(time && refIsActive.current) {
        setTime((time) => time - 1)
      }
    },10)
  }, [time, isActive])

  return (
    <TimerStyled>
      <SecondStyled>{Math.floor(time / 100)}</SecondStyled>
      <MillisecondStyled>{time % 100}</MillisecondStyled>
      <TimerButtonStyled onClick={() => {
        setIsActive(!isActive)
        refIsActive.current = !isActive
      }}/>
      <ResetTimerButtonStyled onClick={() => {
        setIsActive(false)
        refIsActive.current = false
        setTime(props.time / 10)
      }}/>
    </TimerStyled>
  )
}