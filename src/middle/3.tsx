import {FC, useEffect, useState} from "react";
import {LightBulb, TrafficLight} from "./styled";

const LightList = [
  {
    color: 'green',
    duration: 3000,
  },
  {
    color: 'yellow',
    duration: 2000,
  },
  {
    color: 'red',
    duration: 1000,
  },
]

export const testMiddle3 = () => {
  const trafficLight = async (lightList: Array<{color: string, duration: number}>) => {
    let i = 0
    while(i < lightList.length){
      await new Promise(resolve => setTimeout(resolve, lightList[i].duration))
      console.log(lightList[i].color)
      if(i === lightList.length - 1){
        i = 0
      }else{
        i++
      }
    }
  }

  trafficLight(LightList)
}