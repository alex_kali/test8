import styled from "styled-components";

export const LightBulb = styled.div<{color: string, isActive?: boolean}>`
  width: 30px;
  height: 30px;
  background: rgba(0,0,0,0.3);
  margin: 10px;
  transition-duration: 0.3s;
  ${props => props.isActive ? `background: ${props.color};` : ''}
`


export const TrafficLight = styled.div`
  border: 2px solid rgba(0,0,0,0.3);
  background: rgba(0,0,0,0.03);
  width: 50px;
  margin: 100px;
`