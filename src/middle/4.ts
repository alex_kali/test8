export const flatten = (list: Array<any>) => {
  if (!list.length) return;
  let newList = [...list];

  let i = 0;
  while (i < newList.length) {
    if(Array.isArray(newList[i])){
      newList.splice(i, 1, ...newList[i]);
    }else{
      i++
    }
  }

  return newList;

}