import React, {FC, memo, ReactElement, useEffect, useState} from 'react';
import './App.css';
import {flatten} from "./middle/4";
import {Timer} from "./react/7";
import {test1} from "./alpha/1";
import {test2} from "./alpha/2";
import {test4} from "./alpha/4";
import {test6} from "./alpha/6";
import {test7} from "./alpha/7";
import {test8} from "./alpha/8";
import {test9} from "./alpha/9";
import {test10} from "./alpha/10";
import {test12} from "./alpha/12";
import {test13} from "./alpha/13";
import {Test14} from "./alpha/14";
import {Component} from "./alpha/5";
import {testMiddle3} from "./middle/3";

const App = () => {
  console.log(flatten([1, 'any [complex] string', null, function() {}, [1, 2, [3, '4'], 0], [], { a: 1 }]))
  // test1()
  // test2()
  // test4()
  // test6()
  // test7()
  // test8()
  // test9()
  // test10()
  // test12()
  // test13()
  // testMiddle3()
  return (
    <>
      <Timer time={10_000}/>
      {/*<Test14/>*/}
      {/*<Component/>*/}
    </>
  )
};

export default App;
